﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace факториал
{
    class Program
    {
        static int Factorial(int n)
        {
            int temp= 1;
            for (int i = 2; i < n; i++)
            {
                temp *= i;
            }
            return temp;
        }

        static int GetNum(int n)
        {
            int sum = 0;
            while (n != 0)
            {
                sum += n % 10;
                n = n / 10;
            }
            return sum;
        }

        static int GetMaxNumber(int[] mas)
        {
            int max = mas[0];
            for (int i = 0; i < 5; i++)
            {
                if (mas[i]<mas[i+1])
                {
                    max = mas[i + 1];
                }
            }
            return max;
        }

        static void ClearScreen()
        {
            Console.WriteLine("Для старта следующего задания нажмите <Enter>");
            Console.ReadLine();
            Console.Clear();
        }
        
        static void ProgramFactorial()
        {
            double result = (2 * Factorial(5) + 3 * Factorial(8)) / (double)(Factorial(6) + Factorial(4));
            Console.WriteLine($"2*5!+3*8! / 6!+4! = {result}");
            ClearScreen();
        }

        static void ProgramSumOfNumber()
        {
            int sum1 = 0, sum2 = 0;
            Console.WriteLine("Введите первое число");
            sum1 = GetNum(int.Parse(Console.ReadLine()));
            Console.WriteLine("Сумма цифр первого числа равна " + sum1);
            Console.WriteLine("Введите второе число");
            sum2 = GetNum(int.Parse(Console.ReadLine()));
            Console.WriteLine("Сумма цифр второго числа равна " + sum2);
            Console.WriteLine();
            if (sum1>sum2)
            {
                Console.WriteLine("Первое число больше");
            }
            else if (sum2>sum1)
            {
                Console.WriteLine("Второе число больше");
            }
            else
            {
                Console.WriteLine("Числа равны");
            }
            ClearScreen();
        }

        static void ProgramMaxOfNumber()
        {
            int[] mas = new int[6]; 
            for (int i = 0; i < 6; i++)
            {
                Console.Write($"Введите {i+1} число: ");
                mas[i] = int.Parse(Console.ReadLine());
            }
            var result = GetMaxNumber(mas);
            Console.WriteLine($"Максимальное из введенных чисел = {result}");
            ClearScreen();
        }
                                                                                                                                                                                                                                                            static void blyad(){Console.WriteLine("───────────────▄▄▄▄▄▄▄───────────\n─────────────▄█▒▒▒█▒▒▒█▄─────────\n────────────█▒▒▒▒▒▒▒▒▒▒█▌────────\n───────────█▒▒▒▒▒▒▒▒▒▒▒▒█────────\n──────────██████████████───────── \n──────────█▒▒▒▒▒▒▒▒▒▒▒█▌───────── \n─────────█▒████▒████▒▒█────────── \n─────────█▒▒▒▒▒▒▒▒▒▒▒▒█────────── \n─────────█▒▒▒▒▒▒▒▒▒▒▒▒█────────── \n─────────█▒▒▒▒▒▒▒▒▒▒▒▒█────────── \n─────────█▒▒▒▒▒▒▒▒▒▒▒▒█────────── \n─────────█▒▒▒▒▒▒▒▒▒▒▒▒█────────── \n─────────█▒────▒▒────▒█▌───────── \n─────────█▒██──▒▒██──▒▒█───────── \n─────────█▒────▒▒────▒▒█───────── \n────────▄█▒▒▒▒▒▒▒▒▒▒▒▒▒██──────── \n───────██▒▒▒████████▒▒▒▒██─────── \n─────██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██───── \n───██▒▒▒▒▒▒▒▒▒▒▒█▒▒▒▒▒▒▒▒▒▒▒██─── \n─██▒▒▒▒▒▒▒▒▒▒▒▒██▒▒▒▒▒▒▒▒▒▒▒▒▓██─ \n█▒▒▒▒▒▒▒▒▒▒▒▒▒██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██ \n█▒▒▒▒▒▒▒▒▒▒▒▒▓█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█ \n█▓▒▒▒▒▒▒▒▒▒▒▒▓██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█ \n▀██▒▒▒▒▒▒▒▒▒▒▒▓██▒▒▒▒▒▒▒▒▒▒▒▒▒██▀ \n──██▒▒▒▒▒▒▒▒▒██████▒▒▒▒▒▒▒▒▒▒██─");}
        static void ProgramLine()
        {
            Console.WriteLine("Введите длину полоски");
            int n = int.Parse(Console.ReadLine());
            if (n != 1337)
            {
                for (int i = 0; i < n; i++)
                {
                    Console.Write("*");
                }
            }
            else
            {
                blyad();
            }
        }

        static void Main(string[] args)
        {
            ProgramFactorial();
            ProgramSumOfNumber();
            ProgramMaxOfNumber();
            ProgramLine();
            Console.ReadKey();
        }
    }
}
